package com.example.gol

class Cell {

    Integer xCoordinate
    Integer yCoordinate

    Boolean cellState = false

    static hasMany = [neighbourCells: Cell]
    static belongsTo = [board: Board]
    static constraints = {
    }
}
