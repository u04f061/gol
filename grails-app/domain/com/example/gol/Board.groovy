package com.example.gol

class Board {
    Integer horizontalCells
    Integer verticalCells

    static hasMany = [cells: Cell]
    static constraints = {
        horizontalCells range: 10..100
        verticalCells range: 10..100
    }
}
