import com.example.gol.CellMarshallerService

class BootStrap {

    CellMarshallerService cellMarshallerService

    def init = { servletContext ->
        cellMarshallerService.init()
    }
    def destroy = {
    }
}
