class UrlMappings {

    static mappings = {
        "/api/gameOfLife"(controller: 'gameOfLife', action: 'start', method: 'POST')
        "/api/gameOfLife"(controller: 'gameOfLife', action: 'nextGen', method: 'GET')
    }
}
