package com.example.gol

import grails.converters.JSON
import org.springframework.http.HttpStatus

class GameOfLifeController {

    static allowedMethods = [start: 'POST', nextGen: 'GET']
    static responseFormats = ['JSON']

    GameOfLifeService gameOfLifeService

    def start() {
        gameOfLifeService.start(request.JSON)
    }

    def nextGen() {
        Map stats = gameOfLifeService.processNextGeneration()
        render stats as JSON
    }
}
