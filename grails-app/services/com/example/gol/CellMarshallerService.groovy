package com.example.gol

import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CellMarshallerService {

    def init() {
        JSON.registerObjectMarshaller(Cell) { Cell cell ->
            def ret = [:]
            ret['id'] = cell.id
            ret['xCoordinate'] = cell.xCoordinate
            ret['yCoordinate'] = cell.yCoordinate
            ret['cellState'] = cell.cellState

            ret
        }
    }
}
