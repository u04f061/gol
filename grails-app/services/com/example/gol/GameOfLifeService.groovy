package com.example.gol

import grails.transaction.Transactional

@Transactional
class GameOfLifeService {

    Boolean initCells(Board board) {
        for(def i=0; i <board.horizontalCells; i++) {
            for(def j=0; j<board.verticalCells; j++) {
                new Cell(xCoordinate: i, yCoordinate: j, board: board).save(failOnError: true)
            }
        }
        return true
    }

    void populateNeighbour(Cell cell, Integer x, Integer y) {
        Cell neighbour = Cell.findByXCoordinateAndYCoordinate(x+1, y)
        if(neighbour) {
            cell.addToNeighbourCells(neighbour)
        }
    }

    Boolean initRelation() {
        List <Cell> cells = Cell.getAll()

        cells.each { Cell cell ->
            Integer x = cell.xCoordinate
            Integer y = cell.yCoordinate

            populateNeighbour(cell, x+1, y)
            populateNeighbour(cell, x, y+1)
            populateNeighbour(cell, x-1, y)
            populateNeighbour(cell, x, y-1)
            populateNeighbour(cell, x+1, y+1)
            populateNeighbour(cell, x-1, y-1)
            populateNeighbour(cell, x+1, y-1)
            populateNeighbour(cell, x-1, y+1)
            cell.save(failOnError: true)
        }
    }

    def start(Map data) {
        Boolean cellInitStatus = false
        Board board = new Board(data['board'])
        if(board.save(failOnError: true)) {
            cellInitStatus = initCells(board)
        }

        if(cellInitStatus) {
            data['livingCells'].each {
                Cell cell = Cell.findByXCoordinateAndYCoordinate(it.xCoordinate, it.yCoordinate)
                if(cell) {
                    cell.cellState = true
                    cell.save(failOnError: true)
                }
            }

            initRelation()
        }
    }

    def processCellState( Cell cell) {
        List <Cell> livingNeighbours = []
        cell.neighbourCells.each {
            if(it.cellState) {
                livingNeighbours << it
            }
        }
        int aLiveCount = livingNeighbours.size()

        // Each cell with less than 2 neighbours dies
        if(cell.cellState && (aLiveCount < 2)) {
            cell.cellState = false
            cell.save(failOnError: true)
        } else  if(cell.cellState && (aLiveCount > 3)){ // Each cell with 4+ ne. dies of overpopulation
            cell.cellState = false
            cell.save(failOnError: true)
        } else if(!(cell.cellState) && (aLiveCount == 3)) { // dead cell with 3 nb. is populated
            cell.cellState = true
            cell.save(failOnError: true)
        }

        cell
    }

    def processNextGeneration() {
        List <Cell> cells = Cell.getAll()

        cells.each { Cell cell ->
            processCellState(cell)
        }

        Map stats = [:]

        List <Cell> livingCells = Cell.findAllByCellState(true)
        stats['livingCellCount'] = livingCells.size()
        stats['LivingCells'] = livingCells

        stats
    }
}
