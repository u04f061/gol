package com.example.gol

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(GameOfLifeService)
@Mock([Cell, Board])
class GameOfLifeServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Cell with less than two neighbours dies"() {
        given:

        Board board = new Board(horizontalCells: 10, verticalCells: 10)
        List <Cell> neighbours = []
        Cell cell0 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 0, board: board)
        Cell cell1 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 1, board: board)
        neighbours << cell1
        Cell cell2 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 2, neighbourCells: neighbours, board: board)
        Cell result

        when:
        result = service.processCellState(cell2)

        then:
        !result.cellState
    }

    void "Dead cell with exactly 3 alive neighbours is populated" () {
        given:
        Board board = new Board(horizontalCells: 10, verticalCells: 10)
        List <Cell> neighbours = []
        Cell cell0 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 0, board: board)
        neighbours << cell0
        Cell cell2 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 2, board: board)
        neighbours << cell2
        Cell cell3 = new Cell(cellState: true, xCoordinate: 1, yCoordinate: 1)
        neighbours << cell3
        Cell cell1 = new Cell(cellState: false, xCoordinate: 0, yCoordinate: 1, neighbourCells: neighbours, board: board)
        Cell result

        when:
        result = service.processCellState(cell1)

        then:
        result.cellState
    }

    void "Cell with more than three alive neighbours dies" () {
        Board board = new Board(horizontalCells: 10, verticalCells: 10)
        List <Cell> neighbours = []
        Cell cell0 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 0, board: board)
        neighbours << cell0
        Cell cell2 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 2, board: board)
        neighbours << cell2
        Cell cell3 = new Cell(cellState: true, xCoordinate: 1, yCoordinate: 1)
        neighbours << cell3
        Cell cell4 = new Cell(cellState: true, xCoordinate: 2, yCoordinate: 0)
        neighbours << cell4
        Cell cell1 = new Cell(cellState: true, xCoordinate: 0, yCoordinate: 1, neighbourCells: neighbours, board: board)
        Cell result

        when:
        result = service.processCellState(cell1)

        then:
        !result.cellState
    }
}
